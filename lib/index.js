import Backbone from 'backbone';
import { IndexRouter } from './routers/index-router.js';

new IndexRouter();

Backbone.history.start();
