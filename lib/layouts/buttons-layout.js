import Marionette from 'backbone.marionette';
import styles     from '../../css/pages/buttons.css';
import template   from '../../templates/buttons/layout.hbs';

export const ButtonsLayout = Marionette.LayoutView.extend({
  template: template
});
