import Marionette from 'backbone.marionette';
import styles     from '../../css/components/tooltips.css';
import template   from '../../templates/tooltips/layout.hbs';

export const TooltipLayout = Marionette.LayoutView.extend({
  template: template
});
