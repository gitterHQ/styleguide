import Marionette     from 'backbone.marionette';
import template       from '../../templates/index/layout.hbs';
import styles         from '../../css/pages/index.css';

export const IndexLayout = Marionette.LayoutView.extend({
  template: template
});
