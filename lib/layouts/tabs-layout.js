import Marionette from 'backbone.marionette';
import template   from '../../templates/tabs/layout.hbs';
import styles     from '../../css/components/tabs.css';

export const TabsView = Marionette.LayoutView.extend({
  template: template,
});
