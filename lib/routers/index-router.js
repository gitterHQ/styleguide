import $                        from 'jquery';
import Backbone                 from 'backbone';
import styles                   from '../../css/pages/app.css';
import { MenuView }             from '../views/menu-view';
import { MenuTriggerView }      from '../views/menu-trigger-view';
import { IndexLayout }          from '../layouts/index-layout';
import { HeadingsLayout }       from '../layouts/headings-layout';
import { ButtonsLayout }        from '../layouts/buttons-layout';
import { LinksLayout }          from '../layouts/links-layout';
import { ActionElementsLayout } from '../layouts/actions-elements-layout';
import { TooltipLayout }        from '../layouts/tooltip-layout';
import { DropdownsLayout }      from '../layouts/dropdowns-layout';
import { TabsView }             from '../layouts/tabs-layout';
import { ModalsLayout}          from '../layouts/modal-layout';
import { PatternsLayout}          from '../layouts/patterns-layout';

let currentView;

export const IndexRouter = Backbone.Router.extend({

  routes: {

    '':                'onRouteIndex',
    'headings':        'onRouteHeadings',
    'buttons':         'onRouteButtons',
    'links':           'onRouteLinks',
    'action-elements': 'onRouteActionElements',
    'tooltips':        'onRouteTooltips',
    'dropdowns':       'onRouteDropdowns',
    'tabs':            'onRouteTabs',
    'modals':          'onRouteModals',
    'patterns':        'onRoutePatterns',

  },

  initialize: function() {
    this.menu = new MenuView({
      el: '[data-component=menu]',
    });
    this.menu.render();

    this.menuTrigger = new MenuTriggerView({
      el: '[data-component=menu-trigger]',
    });
  },

  onRouteIndex: function() {
    this._renderView(IndexLayout);
  },

  onRouteHeadings: function() {
    this._renderView(HeadingsLayout);
  },

  onRouteButtons: function() {
    this._renderView(ButtonsLayout);
  },

  onRouteLinks: function() {
    this._renderView(LinksLayout);
  },

  onRouteActionElements: function() {
    this._renderView(ActionElementsLayout);
  },

  onRouteTooltips: function() {
    this._renderView(TooltipLayout);
  },

  onRouteDropdowns: function() {
    this._renderView(DropdownsLayout);
  },

  onRouteTabs: function() {
    this._renderView(TabsView);
  },

  onRouteModals: function (){
    this._renderView(ModalsLayout);
  },

  onRoutePatterns: function (){
    this._renderView(PatternsLayout);
  },

  _renderView: function(ViewConstructor) {

    //delete a current view
    var oldView = currentView;

    //make a new view
    currentView = new ViewConstructor({
      el: domElementFactory(),
    });

    //render the view
    currentView.render();

    //append the div into the application container
    $('[data-component=application]').append(currentView.el);

    //animate in
    setTimeout(() => {
      if (currentView && currentView.$el) currentView.$el.addClass('active');
      if (oldView && oldView.$el) oldView.$el.addClass('inactive');
    }, 50);

    //destroy old view
    setTimeout(() => {
      if (oldView && oldView.destroy) oldView.destroy();
      oldView  = null;
    }, 750);

  },

});

function domElementFactory() {
  let div = document.createElement('div');
  div.className = 'layout-container';
  return div;
}
