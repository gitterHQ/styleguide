'use strict';

var express = require('express');
var path = require('path');
var webpack = require('webpack');
var webpackDevMiddleWare = require('webpack-dev-middleware');
var gitterEnv = require('@gitterhq/env');
var webpackConfig = require('./webpack.config.js');

var env = gitterEnv.create('./config');
var winston = env.logger;

process.on('unhandledRejection', (reason, p) => {
  winston.error('unhandledRejection' + reason, { exception: reason });
});

process.on('uncaughtException', function(err) {
  winston.error('uncaughtException: ' + err, { exception: err });
});


var port = process.env.PORT || 9090;
var app = express();

app.get('/', function(req, res){
  res.sendFile(path.resolve(__dirname, './index.html'));
});

app.get('/normalize.css', function(req, res){
  res.sendFile(path.resolve(__dirname, './node_modules/normalize.css/normalize.css'));
});

app.use('/build', webpackDevMiddleWare(webpack(webpackConfig)));

app.listen(port, function(){
  console.log('App listening on ' + port);
});
