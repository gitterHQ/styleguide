const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const getPostcssStack = require('./postcss-stack');


module.exports = {
  devtool: 'source-map',
  entry: {
    app: path.resolve(__dirname, './lib/index.js'),
  },
  output: {
    path: path.resolve(__dirname, './build'),
    filename: '[name].js',
    publicPath: '/build/',
  },
  module: {
    rules: [
      {
        test: /.css$/,
        use: [
          { loader: MiniCssExtractPlugin.loader },
          { loader: 'css-loader', options: { sourceMap: true } },
          { loader: 'postcss-loader', options: { plugins: getPostcssStack(webpack) } }
        ]
      },
      {
        test:    /.js$/,
        include: path.resolve(__dirname, './lib'),
        loader:  'babel-loader',
      },
      {
        test:    /.hbs$/,
        include: path.resolve(__dirname, './templates'),
        loader:  'handlebars-loader',
        query:   {
          //helpers
        },
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'styles.css',
    })
  ],
};
