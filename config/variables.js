module.exports = {
  'base-text-color': '#333333',

  'clouds-mock': '#783abd',
  'clouds-content': '#fffefe',

  'caribbean': '#46bc99',
  'caribbean-light': '#cfe9df',
  'caribbean-lightest': '#e1eee8',

  'jaffa': '#f68d42',
  'jaffa-light': '#f3eae0',

  'ruby': '#ed1965',
  'ruby-border': '#f5e0dc',
  'ruby-dark': '#d5145b',

  'pampas': '#f2f0ed',
  'pampas-border': '#d5d5d5',

  'thunder': '#3a3133',

  'mountain': '#949494',
  'mountain-dark': '#858585',

  'gitlab-orange': '#fc6d26',
  'facebook-blue': '#3B5998',
  'twitter-blue': '#55ACEE',
  'linkedin-blue': '#0077b5',
  'google-red': '#DC4E41',

  'h1-font-size': '3.789rem',
  'h1-line-height': '6.1rem',

  'h2-font-size': '2.843rem',
  'h2-line-height': '4.5rem',

  'h3-font-size': '2.132rem',
  'h3-line-height': '3.4rem',

  'h4-font-size': '1.6rem',
  'h4-line-height': '2.6rem',

  'h5-font-size': '1.2rem',
  'h5-line-height': '1.9rem',

  'activity-border-radius': '.7em',
  'activity-padding': '.4em 1.2em',

  'timestamp-tooltip-border-radius': '.7rem',

  'fineprint-color': 'grey',
  'fineprint-font-size': '1.25rem',
  'fineprint-line-height': '1.4rem'
};
